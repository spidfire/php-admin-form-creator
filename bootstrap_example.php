<?php
session_start();
  $ROOT_LINK = "bootstrap_example.php?";
 include "config.php";
  include "formengine/main.php";
  $use_login = false;
 

  if($use_login === true){    
    if(isset($_GET['logout'])){
      unset($_SESSION['user']);
    }
    if(isset($_POST['submit'])){
      $res = fastSelect('user',array('username'=>$_POST['username'], 'password'=>md5($_POST['password'])));
      if(count($res)> 0){
        $_SESSION['user'] = true;
        $_SESSION['id'] = $res['id'];
      }else{
        echo "Faulty login!!";
      }
    }
  }
  
  // You can use it by adding table names in the array
  $generate = array();
  // exmple: $generate = array('user','items');
 
  adminGenerator($generate);

    
    $pages = array();

    // Place here your generated code!
   $pages['items'] = array();
    $pages['items'] = array();
    $pages['items']['name'] = 'items';//The title of this table (can be everything)
    $pages['items']['table'] = 'items';// sql table name
    $pages['items']['pk'] = 'id';// the primairykey (usualy id)
    // id 
    $field = new admin_pk('id', 'id');
    $pages['items']['fields'][] = $field;
    // name 
    $field = new admin_input('name', 'name');
    $pages['items']['fields'][] = $field;
    // text 
    $field = new admin_textarea('text', 'text');
    $pages['items']['fields'][] = $field;
    // created_on 
    $field = new admin_date_select('created_on', 'created_on');
    $pages['items']['fields'][] = $field;
    // user_id 
    $field = new admin_rever('user_id', 'user_id');
    $field->show_sqlcode = "select username as output from user where id = %s ";
    $field->update_options = "select username as name, id as value from user ";
    $pages['items']['fields'][] = $field;
    // type 
    $field = new admin_choice_select('type', 'type',admin_choice::$five_star);
    $pages['items']['fields'][] = $field;
    // enabled 
    $field = new admin_choice('enabled', 'enabled',admin_choice::$enabled_disabled);
    $pages['items']['fields'][] = $field;
    // password 
    $field = new admin_password('password', 'password');
    $pages['items']['fields'][] = $field;


    $pages['user'] = array();


    $pages['user']['name'] = 'user';//The title of this table (can be everything)
    $pages['user']['table'] = 'user';// sql table name
    $pages['user']['pk'] = 'id';// the primairykey (usualy id)
    // id 
    $field = new admin_pk('id', 'id');
    $pages['user']['fields'][] = $field;
    // username 
    $field = new admin_input('username', 'username');
    $pages['user']['fields'][] = $field;
    // password 
    $field = new admin_password('password', 'password');
    $field->hide_from_show = true;
    $pages['user']['fields'][] = $field;
    // admin 
    $field = new admin_choice('admin', 'admin',admin_choice::$yes_no);
    $pages['user']['fields'][] = $field;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>PHP admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
 <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo $ROOT_LINK ?>">Admin</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="<?php echo $ROOT_LINK ?>">Home</a></li>
             <?php
             if(isset($_SESSION['user']) || $use_login === false){
               foreach ($pages as $key => $value) {
		            //<a href="'.$ROOT_LINK.'i=%s">Insert</a>
		            echo sprintf('<li><a href="'.$ROOT_LINK.'e=%1$s"> %2$s</a></li>',$key,$value['name']) ;
		        }
             ?>
             

             <?php
             if($use_login === true){
              echo '<li><a href="?logout=true">Logout</a></li>';
             }
           }

           ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
 

    <div class="container">
<?php
if(!isset($_SESSION['user']) && $use_login === true){

    echo '<form method="POST" action="'.$ROOT_LINK.'">
    username: <input type="text" name="username"/><br/>
    password: <input type="password" name="password"/><br/>
    <input type="submit" name="submit" value="login"/>
    </form>';
    die();
  }else{
	 formengine($pages,$ROOT_LINK);
	
  }


?>
    </div> <!-- /container -->


  </body>