
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `created_on` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(64) NOT NULL,
  `enabled` int(11) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


INSERT INTO `items` (`id`, `name`, `text`, `created_on`, `user_id`, `type`, `enabled`, `password`) VALUES
(1, 'tsetst', 'sdaaddsa', '2012-10-03 23:32:00', 2, '3', 1, 'd41d8cd98f00b204e9800998ecf8427e');



CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(33) NOT NULL,
  `admin` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;



INSERT INTO `user` (`id`, `username`, `password`, `admin`) VALUES
(1, 'spidfire', '4773e610ef1b43b1f3511774a43f85f0', 1),
(2, 'spidfire22', '4773e610ef1b43b1f3511774a43f85f0', 1);