<?php


function adminGenerator($generate){
    /* generator */   
    // You can use it by adding table names in the array
    foreach ($generate as $key => $value) {
        echo '$pages[\''.$value.'\'] = array();<br/>';
        echo '$pages[\''.$value.'\'][\'name\'] = \''.$value.'\';//The title of this table (can be everything)<br/>';
        echo '$pages[\''.$value.'\'][\'table\'] = \''.$value.'\';// sql table name<br/>';
        echo '$pages[\''.$value.'\'][\'pk\'] = \'id\';// the primairykey (usualy id)<br/>';
        foreach(q('Show fields  FROM '. $value) as $fields){
            echo '// '.$fields['Field'].' <br/>';
            echo '$field =  new admin_input(\''.$fields['Field'].'\', \''.$fields['Field'].'\');<br/>';
            echo '$pages[\''.$value.'\'][\'fields\'][] = $field;<br/>';
        }
    }

}
function s($text) {
    return htmlentities($text);
}

function a($text) {
    if (get_magic_quotes_gpc() == 1) {
        return ($text);
    } else {
        return (addslashes($text));
    }
}

function q($sql) {

    if ($query = mysql_query($sql)) {
        if (is_bool($query)) {
            return $query;
        }
        $return = array();
        while ($r = mysql_fetch_assoc($query)) {
            $return[] = $r;
        }
        return $return;
    } else {
        die("there has been a error ".mysql_error());
    }
}



function queryToArray($sql) {
    if ($query = mysql_query($sql)) {
        $result = array();
        if (@mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                foreach($row as $id => $val) {
                    $row[$id] = stripslashes($val);
                }
                $result[] = $row;
            }

        }
        return $result;
    } else {
        die("Error ".mysql_error());
    }

}

function fastEdit($table, $variables, $where) {
    $result_names = array();
    $where_names = array();
    foreach($variables as $name => $value) {
        $result_names[] = " `".$name."` = '".mysql_real_escape_string((string)$value)."' ";
    }
    foreach($where as $name => $value) {
        $where_names[] = " `".$name."` = '".mysql_real_escape_string((string)$value)."' ";
    }

    $sql = "UPDATE `".$table."` SET";
    $sql .= "".implode(",", $result_names)."";
    $sql .= "WHERE";
    $sql .= "(".implode(" AND ", $where_names).")";

    queryToArray($sql);

}
function fastInsert($table, $variables) {
    $result_names = array();
    $result_values = array();
    foreach($variables as $name => $value) {
        $result_names[] = "`".(string)$name."`";
        $result_value[] = "'".mysql_real_escape_string((string)$value)."'";
    }


    $sql = "INSERT INTO `".$table."` ";
    $sql .= "(".implode(",", $result_names).")";
    $sql .= "VALUES";
    $sql .= "(".implode(",", $result_value).")";
    queryToArray($sql);

}

function fastDelete($table, $where = array()) {

    $where_names = array();
    foreach($where as $name => $value) {
        if (is_array($value)) {
            $where_names[] = " `".$name."` ".$value[0]." ".$value[1]." ";
        } else {
            $where_names[] = " `".$name."` = '".mysql_real_escape_string($value)."' ";
        }
    }

    $sql = "DELETE from `".$table."`";
    if (count($where_names) > 0) {
        $sql .= " WHERE ".implode(" AND ", $where_names)."";
    }
    return queryToArray($sql);

}


function fastSelect($table,$where = array(),$orderby = null,$direction = "asc"){
    
        $where_names = array();     
        foreach($where as $name => $value){
            if(is_array($value)){
                $where_names[] = " `".$name."` ".$value[0]." ".$value[1]." ";
            }else{
            $where_names[] = " `".$name."` = '".mysql_real_escape_string($value)."' ";
            }
        }

        $sql = "SELECT * from `".$table."`";
        if(count($where_names) > 0){
        $sql .= " WHERE ".implode(" AND ",$where_names)."";
        }
        if($orderby != null){
            $sql .= "Order by ".$orderby." ".$direction;
        }
        return queryToArray($sql);

}