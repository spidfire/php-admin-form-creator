<?php
//default input field
class admin_input{
        var $sql, $name, $settings,$table;
        var $description;

        var $hide_from_update = false;
        var $hide_from_show = false;
        var $hide_from_insert = false;

        function __construct($sql, $name, $settings=array()){
            $this->sql = $sql;
            $this->name = $name;
            $this->settings = $settings;
        }
        function setTable($table){
        	$this->table = $table;
        }
        function is_invalid(){
            // if($this->val() == 'test'){
            //     return $this->name." is test!!";
            // }
            return false;
        }
        function ins_sql($key,$value){
            $key[] =  "`" . $this->sql . "`";
            $value[] = "'". a($this->val()) . "'";
            return array($key,$value);
        }
        function upd_sql($value){
            $value[] =  "`" . $this->sql . "` = '". a($this->val()) . "'";
            return $value;
        }
        function val(){
            return isset($_POST[$this->sql]) ? $_POST[$this->sql] : null;
        }
        function label(){
            return htmlspecialchars($this->name);
        }
        function show($values){
            return htmlspecialchars($values[$this->sql]);
        }
        function update($value){
            return sprintf("<input type='text' name=\"%s\" value=\"%s\" />",a($this->sql), s($value));
        }
        function insert(){
            return $this->update($this->val());
        }
    }

 // base class that doesnt do anything (for extending)   
 class admin_input_nothing extends admin_input{
       
        function is_invalid(){return true;}
        function ins_sql($key,$value){return array($key,$value);}
        function upd_sql($value){return $value;}
        function val(){return isset($_POST[$this->sql]) ? $_POST[$this->sql] : null;}
        function label(){return htmlspecialchars($this->name);}
        function show($values){return "";}
        function update($value){return "";}
        
    }
    // depricated
 class admin_settings extends admin_input_nothing{
       
        function update($value){
            $value = $value != "" ? $value : $this->settings['value'];
            return sprintf("<input type='text' name=\"%s\" value=\"%s\" />",a($this->sql), s($value));
        }
        
    }
  // input field with a preset value
 class admin_preset extends admin_input{
       
        function update($value){
            $value = $value != "" ? $value : $this->settings['value'];
            return sprintf("<input type='text' name=\"%s\" value=\"%s\" />",a($this->sql), s($value));
        }
        
    }
    class admin_number extends admin_input{
        function is_invalid(){
            if(ctype_digit($this->val())){
                return $this->name." is not a number!!";
            }
            return false;
        }
        
    }
    class admin_date extends admin_input{
        function is_invalid(){
            if(!preg_match('/[0-2][0-9]{3}-(1[0-2]|0[1-9])-(3[01]|[0-2][1-9])/im',$this->val())){
                return $this->name." not a good date use (YYYY-MM-DD)!<Br/> And it must be a valid date!!";
            }
            return false;
        }
        function update($value){
            return parent::update($value). "<i>(use: YYYY-MM-DD)</i>";
        }
    }
class admin_rever extends admin_choice_select{
       var $show_sqlcode = 'select "value:  %s " as output';
       var $update_options = 'select "not yet set" as name, "fiets" as value ';
       
        function show($values){
            $sql = sprintf($this->show_sqlcode,a($values[$this->sql]));

                
            if($r = q($sql)){
                return $r[0]['output'];
            }
            
            return "";
        }

        function update($value){
            $r = q($this->update_options);
            $output = "";

            foreach ($r as $key => $result) {
                 $this->settings[$result['value']] = $result['name'];
            }

            return parent::update($value);
            
        }
    }

    class admin_choice_select extends admin_input{
       function update($value){
        $html = '';
            foreach($this->settings as $setting => $radiovalue){
            
                if($value == $setting){
                    $html .= sprintf("<option selected='selected' value='%s'>%s</option>",a($setting), a($radiovalue));                    
                }else{
                    $html .= sprintf("<option value='%s'>%s</option>",a($setting), a($radiovalue));
                    
                }
            }
            return "<select name='".a($this->sql)."'>".$html."</select>";
        }   

        function show($values){
            $len = 10;
            $txt = htmlspecialchars($values[$this->sql]);
                return strlen($txt)>$len ? substr($txt, 0,$len)."..." : $txt;
        }
    }
    class admin_choice extends admin_input{
    	var $max_show_chars = 10;
    	static $yes_no = array('1' =>'yes', '0' => 'no');
    	static $enabled_disabled = array('1' =>'enabled', '0' => 'disabled');
    	static $five_star = array('1' =>'1 star','2' =>'2 stars','3' =>'3 stars','4' =>'4 stars','5' =>'5 stars');

       function update($value){
        $html = '';
            foreach($this->settings as $setting => $radiovalue){
                if($value == $setting){
                    $html .= sprintf("%s<input type='radio' name='%s' checked='checked' value='%s'><br/>",$radiovalue, a($this->sql), htmlspecialchars($setting));                    
                }else{
                    $html .= sprintf("%s<input type='radio' name='%s' value='%s'><br/>",$radiovalue, a($this->sql), htmlspecialchars($setting));
                    
                }
            }
            return $html;
        }   

        function show($values){
            $len  = $this->max_show_chars;
            $txt = htmlspecialchars($values[$this->sql]);
                return strlen($txt)>$len ? substr($txt, 0,$len)."..." : $txt;
        }
    }


    class admin_textarea extends admin_input{
       function update($value){
            return sprintf("<textarea style='width:400px;height:200px' name=\"%s\" >%s</textarea>",a($this->sql), htmlspecialchars($value));
        }

        function show($values){
            $len = 10;
            $txt = htmlspecialchars($values[$this->sql]);
                return strlen($txt)>$len ? substr($txt, 0,$len)."..." : $txt;
        }
    }
    class admin_password extends admin_input{
       function ins_sql($key,$value){
            $key[] =  "`" . $this->sql . "`";
            $value[] = "'". md5(a($this->val())) . "'";
            return array($key,$value);
        }
        function upd_sql($value){
            if($this->val() != ''){
                $value[] =  "`" . $this->sql . "` = '". md5(a($this->val())) . "'";
                
            }
            return $value;
        }
        function update($value){
            return sprintf("<input type='password' name=\"%s\" />",a($this->sql));
        }
        function show($values){
            
                return "[password]";
        }

    
    }
class admin_date_select extends admin_input{
        function ins_sql($key,$value){
            $key[] =  "`" . $this->sql . "`";
            $value[] = "'". a($this->getDate()) . "'";
            return array($key,$value);
        }
        function upd_sql($value){
            

            $value[] =  "`" . $this->sql . "` = '". a($this->getDate()) . "'";
            return $value;
        }
        function iss($name){
            return isset($_POST[$name]) ? $_POST[$name] : 0;
        }
        function getDate(){
            $day = $this->iss($this->sql."_day");
            $month = $this->iss($this->sql."_month");
            $year = $this->iss($this->sql."_year");

            $hour = $this->iss($this->sql."_hour");
            $minute = $this->iss($this->sql."_minute");
            //2012-09-19 05:33:31
            return sprintf("%04d-%02d-%02d %02d:%02d:00",$year,$month,$day,$hour,$minute);
        }
        function select_($name, $min,$max,$val){
            $html = ""; 
            
            for($i =$min;$i <= $max;$i++){
                if($val == $i){
                    $html .=  sprintf("<option selected=\"selected\" value=\"%d\" />%s</option>",$i,$i);                    
                }else{
                     $html .=  sprintf("<option value=\"%s\" />%s</option>",$i,$i);  
                }
            }

            return sprintf("<select type='text' style='width:70px' name=\"%s\" />%s</select>",$this->sql."_".$name,$html);

        }
        function update($value){  
            if(isset($_POST[$this->sql."_day"])){
                $value = $this->getDate();
            }        
            $datetime = explode(" ", $value);
            $date = explode("-", $datetime[0]);
            $time = explode(":", $datetime[1]);
            //2012-09-19 05:33:31
            $html = "<a href='Javascript:document.getElementById(\"date".$this->sql."\").style.display
             = \"\"'>";
            $html .= "Datum: ".$date[2]."-".$date[1]."-".$date[0]." ".$time[0].":".$time[1]."<br/>";
            $html .= " (Click to edit)</a>";
            $html .= '<div id="date'.$this->sql.'" style="display:none">';
            $html .= 'Edit: '.$this->select_('day',1,31,(int)$date[2]);
            $html .= '-'.$this->select_('month',1,12,(int)$date[1]);
            $html .= '-'.$this->select_('year',1900,2020,(int)$date[0])."(d-m-j)";


            $html .= '<br/>Tijd: '.$this->select_('hour',0,24,(int)$time[0]);
            $html .= ':'.$this->select_('minute',0,60,(int)$time[1])."(h:m)";
            $html .= "</div>";

            return $html;
        }
         function insert(){            
            return $this->update(date("Y-m-d H:i:s"));
        }
    }

    class admin_url extends admin_input{
        function is_invalid(){
            if(!preg_match('%^(?:http|https)://.+$%i',$this->val())){
                return $this->name." is not an url!!";
            }
            return false;
        }
        function update($value){
            return sprintf("<input type='text' style='width:400px' name=\"%s\" value=\"%s\" />(",a($this->sql), s($value));
        }
        
    }
    
    
    class admin_pk extends admin_number{
        function is_invalid(){
            if(ctype_digit($this->val())){
                return $this->name." is not a number!!";
            }
            return false;
        }
        function ins_sql($key,$value){           
            return array($key,$value);
        }
        function upd_sql($value){           
            return $value;
        }
        function insert(){
            return "{automatic}";
        }
        function update($value){
            return htmlentities($value);
        }
    }


class admin_image extends admin_input{

        var $dir = 'images/';
        var $extdir = 'images/';

        var $allow_nofile = true;
         function ins_sql($key,$value){

            $data = $this->uploadImage();
            if($data !== false){                
                $key[] =  "`" . $this->sql . "`";
                $value[] = "'". $data . "'";                
            }
            
            return array($key,$value);
        }
        function upd_sql($value){
            $data = $this->uploadImage();
            if($data !== false){
                $value[] =  "`" . $this->sql . "` = '".$data . "'";
                
            }
            return $value;
        }
         function uploadImage(){
            $extension = end(explode(".", $_FILES[$this->name]["name"]));
            $dir = getcwd()."/".$this->dir;
            if(!is_writable($dir)){
                echo "Dir is not writable ".($dir);
                return "" ;
            }
            $id = 1;
            while(file_exists($dir."custom".$id.".".$extension)){            
                $id++;

            }
           
            if($_FILES[$this->name]["error"] == UPLOAD_ERR_NO_FILE && $this->allow_nofile == true){

                return false;

            }elseif($_FILES[$this->name]["error"] > 0){
                $upload_errors = array( 
                UPLOAD_ERR_OK        => "No errors.", 
                UPLOAD_ERR_INI_SIZE    => "The file is to big than ".ini_get('upload_max_filesize').".", 
                UPLOAD_ERR_FORM_SIZE    => "Larger than form MAX_FILE_SIZE.", 
                UPLOAD_ERR_PARTIAL    => "Partial upload.", 
                UPLOAD_ERR_NO_FILE        => "No file.", 
                UPLOAD_ERR_NO_TMP_DIR    => "No temporary directory.", 
                UPLOAD_ERR_CANT_WRITE    => "Can't write to disk.", 
                UPLOAD_ERR_EXTENSION     => "File upload stopped by extension.", 
                UPLOAD_ERR_EMPTY        => "File is empty." // add this to avoid an offset 
              ); 
                echo $upload_errors[$_FILES[$this->name]["error"]];
                die();
            }else{
                if(move_uploaded_file($_FILES[$this->name]["tmp_name"],
                    $dir."custom".$id.".".$extension) == false){
                   
                    echo "Error while uploading!!"; 
                }
                return $this->extdir."custom".$id.".".$extension; 
            }
            

        }

        function to_url($val){
            return  $val;

        }
       function update($value){
            
            $input = sprintf("Change image: <input type='file' name=\"%s\" value=\"%s\" /><br/>",a($this->sql), s($value));
            //$input = "";
            return $input.sprintf("<input type='hidden' name=\"%s\" value=\"%s\" />",a($this->sql), s($value)).
            sprintf("<img src='%s' width='200' />", $this->to_url($value)).sprintf("<br/><a href='%s'>Download</a>",$this->to_url($value));

        }

        function show($values){
           return sprintf("<a href='%s'>Download</a>",$this->to_url($values));
        }
    }



    class admin_money extends admin_input{
        var $sign = "&euro;";
        var $dotsign = ".";

       function update($value){
        
            $html = '';
            $euro = 0;
            $cent = 0;
            if (preg_match('/^([0-9]*?)([0-9]{0,2})?$/m', $value, $regs)) {
                $euro = $regs[1];
                $cent = $regs[2];
            }
            $html .= $this->sign;

            $html .= sprintf("<input type='text' name='%s' style='width:50px' value='%d'>",$this->sql, a($euro));                    
            $html .= $this->dotsign;
            $html .= sprintf("<input type='text' name='%s_cent' style='width:50px' value='%d'><br/>",$this->sql, a($cent));                    
               
            return $html;
        }   
         function val(){
            if(isset($_POST[$this->sql])){
                
                $euro = $_POST[$this->sql];
                $cent = $_POST[$this->sql."_cent"];
                return sprintf('%d%02d',$euro,$cent);
            }else{
                return null;
            }
           
        }
        function show($values){
            $euro = 0;
            $cent = 0;
            if (preg_match('/^([0-9]*?)([0-9]{0,2})?$/m', $values[$this->sql], $regs)) {
                $euro = $regs[1];
                $cent = $regs[2];
            }
            
            //var_dump($euro,$cent); 
            return $this->sign." ".$euro.$this->dotsign.$cent;
        }
    }