<?php
/* formengine created by Djurre de Boer <info@djurredeboer.nl> */

 include("functions.php");
 include("input_types.php");
 /**
  * formengine add pages and a rootlink
 */
function formengine($pages,$ROOT_LINK){
        
    if(isset($_GET['i'])){

        $pagecode = utf8_encode($_GET['i']);
        formengine_input($pages,$ROOT_LINK,$pagecode);
    }elseif(isset($_GET['e'])){
        $pagecode = utf8_encode((string)$_GET['e']);

        if(!isset( $pages[$pagecode])){
            die("pagecode does not exist, go back <a href='".$ROOT_LINK."'>Back</a>");
        }

        if(!isset($_GET['id'])){
            formengine_show($pages,$ROOT_LINK,$pagecode);
        }else{
            formengine_edit($pages,$ROOT_LINK,$pagecode);
        }
    }else{
        foreach ($pages as $key => $value) {
            echo sprintf(' <a href="'.$ROOT_LINK.'e=%1$s"><button>Show</button></a> %2$s<br/>',$key,$value['name']) ;
        }

    }
}
    



function formengine_input($pages,$ROOT_LINK,$pagecode){

        if(!isset( $pages[$pagecode])){die("pagecode does not exist, go back <a href='".$ROOT_LINK."'>Back</a>");}
        $p = $pages[$pagecode];

        $name = isset($p['name']) ? $p['name'] : $pagecode;
        echo "<h2>Add: ".htmlspecialchars($name)."</h2>";
        echo "<a href='".$ROOT_LINK."'>Back</a></a>";
        $table = $p['table'];



        if(isset($_POST["submit_".$pagecode])){
            $allvalid = true;
            $ins_key = array();
            $ins_val = array();
            $errors = "";
            foreach ($p['fields'] as $value) {

                    $value->setTable($p['table']);
                if($error = $value->is_invalid()){
                    $allvalid = false;
                    $errors .= "<font color='red'>".$error."</font>";
                }else{
                    list($ins_key,$ins_val) = $value->ins_sql($ins_key,$ins_val);
                }                
            }
            if($allvalid == true){
                $keys = implode(" , ", $ins_key);
                $vals = implode(" , ", $ins_val);
                $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)",$table,$keys,$vals);
                if(q($sql) == true){
                    $url = $ROOT_LINK."e=".$pagecode. "&id=".mysql_insert_id();
                    echo "New record inserted click <a href='".$url."'>here</a> to see it";
                    unset($_POST);
                }
            }else{
                echo "<fieldset><h3>There are errors</h3>".$errors."</fieldset>";
            }
            
        }
        
        echo "<form  enctype='multipart/form-data' method='post'><table  class=\"table table-striped\">";
        foreach ($p['fields'] as $value) {
                if($value->hide_from_insert == true){
                    continue;
                }
                    $value->setTable($p['table']);
            if($error = $value->is_invalid()){
                echo "<tr><td colspan='2'><font color='red'>". $error."</font></td></tr>";
            }
            echo "<tr><td>".$value->label(). "</td><td>" . $value->insert()."".($value->description != null ?"<br/>(".$value->description.")" :"")."</td></tr>";
        }
        echo "<tr><td colspan='2'><input type='submit' value='Insert' name='submit_".$pagecode."'/></td></tr>";
        echo "</table>";
       
        echo "</form>";
}

// ==================================================================
//
// Edit part of the formengine
//
// ------------------------------------------------------------------

function formengine_show($pages,$ROOT_LINK,$pagecode){
    
            if(isset($_GET['d'])){
                if(isset($_GET['delid']) && strlen($_GET['delid'])>0 && ctype_digit($_GET['delid'])){
                    if($_GET['d'] == 'ask'){
                        echo "<h2 style='color:#F00'>Weet u zeker dat u dit wilt doen? <a href='".$ROOT_LINK."e=".$pagecode."&d=do&delid=".$_GET['delid']."'>Ja</a><h2>";
                    }else{

                    $editid = utf8_encode($_GET['delid']);
                    $p = $pages[$pagecode];
                    $table = $p['table'];
                    //var_dump($table,$p['pk'],a($editid));
                     $sql = sprintf("DELETE FROM `%s` WHERE `%s` = '%s' LIMIT 1 ",$table,$p['pk'],a($editid));
                            //echo $sql;
                            if(q($sql) == true){                       
                                echo "<Br/> <b>Record deleted.</b>";
                            }
                    }
                }

            }
            $p = $pages[$pagecode];
            $table = $p['table'];
            $name = isset($p['name']) ? $p['name'] : $pagecode;
            $listselect = isset($p['listselect']) ? $p['listselect'] : sprintf("Select * from `%s`",$table);
            
            echo "<h2>Show: ".htmlspecialchars($name)."</h2>";  
            echo "<a href='".$ROOT_LINK."'>Back</a></a>";          
            echo "<table  class=\"table table-striped table-hover\">";            
            $first = true;
            foreach(q($listselect) as $row){
                if($first){
                    echo "<tr><th>#</th>";
                        foreach ($p['fields'] as $value) {
                            if($value->hide_from_show == true){
                                continue;
                            }
                    $value->setTable($p['table']);
                            echo "<th>".$value->name."</th>";
                        }
                    echo"</tr>";
                    $first = false;
                }
                echo "<tr><td><a href='".$ROOT_LINK."e=".$pagecode."&id=".$row[$p['pk']]."'>Edit</a>
                <a href='".$ROOT_LINK."e=".$pagecode."&d=ask&delid=".$row[$p['pk']]."'>Delete</a></td>";
                foreach ($p['fields'] as $value) {
                    if($value->hide_from_show == true){
                        continue;
                    }
                    $value->setTable($p['table']);
                    echo "<td>".$value->show($row)."</td>";
                }
                echo "</tr>";
            }   
            echo "</table><a href='".$ROOT_LINK."i=".$pagecode."'>Insert</a></a>";  
     
}
// ==================================================================
//
// SHOW part of the formengine
//
// ------------------------------------------------------------------

function formengine_edit($pages,$ROOT_LINK, $pagecode){
    $editid = utf8_encode($_GET['id']);
            $p = $pages[$pagecode];


            $name = isset($p['name']) ? $p['name'] : $pagecode;
            $whereselect = isset($p['whereselect']) ? $p['whereselect'] : "Select * from `%s` where `%s` = '%s' ";
            echo "<h2>Edit: ".htmlspecialchars($name)."</h2>";
            echo "<a href='".$ROOT_LINK."e=".$pagecode."'>Back</a></a>";
            $table = $p['table'];
            $query = sprintf($whereselect,
                            a($table),
                            a($p['pk']),
                            a($editid));
            $result = q($query);
            $r = $result[0];
            if(isset($_POST["submit_".$pagecode])){
                $allvalid = true;                
                $ins_val = array();
                $errors = "";
                foreach ($p['fields'] as $value) {
                    if($value->hide_from_update == true){
                        continue;
                    }

                    $value->setTable($p['table']);
                    if($error = $value->is_invalid()){
                        $allvalid = false;
                        $errors .= "<font color='red'>".$error."</font>";
                    }else{
                        $ins_val = $value->upd_sql($ins_val);
                    }                
                }
                if($allvalid == true){
                    $vals = implode(" , ", $ins_val);
                    $sql = sprintf("UPDATE `%s` SET %s WHERE `%s` = '%s' ",$table, $vals,$p['pk'],a($editid));
                    
                    if(q($sql) == true){                       
                        echo "<Br/> <b>Record edited.</b>";
                    }
                }else{
                    echo "<fieldset><h3>There are errors</h3>".$errors."</fieldset>";
                }
                
            }
            
            echo "<form  enctype='multipart/form-data' method='post'><table  class=\"table table-striped\">";
            foreach ($p['fields'] as $value) {
                if($value->hide_from_update == true){
                    continue;
                }
                    $value->setTable($p['table']);
                if(isset($_POST["submit_".$pagecode])){
                    if($error = $value->is_invalid()){
                        echo "<tr><td colspan='2'><font color='red'>". $error."</font></td></tr>";
                    }
                    echo "<tr><td>".$value->label(). "</td><td>" . $value->insert()."".($value->description != null ?"<br/>(".$value->description.")" :"")."</td></tr>";
                }else{
                    echo "<tr><td>".$value->label(). "</td><td>" . $value->update($r[$value->sql])."".($value->description != null ?"<br/>(".$value->description.")" :"")."</td></tr>"; 
                }
                
            }
            echo "<tr><td colspan='2'><input type='submit' class='btn btn-primary' value='Edit' name='submit_".$pagecode."'/></td></tr>";
            echo "</table>";
           
            echo "</form>";
}

